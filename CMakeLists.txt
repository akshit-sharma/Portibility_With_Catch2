cmake_minimum_required(VERSION 3.10)
project(main_program)
project(main_test)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(DEFAULT_OUT_OF_SOURCE_FOLDER "BUILD")

if (${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR})
    message(WARNING "In-source builds not allowed. CMake will now be run with arguments:
        cmake -H. -B${DEFAULT_OUT_OF_SOURCE_FOLDER}
")

    # Run CMake with out of source flag
    execute_process(
            COMMAND ${CMAKE_COMMAND} -H. -B${DEFAULT_OUT_OF_SOURCE_FOLDER}
            WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})

    # Cause fatal error to stop the script from further execution
    message(FATAL_ERROR "CMake has been ran to create an out of source build.
This error prevents CMake from running an in-source build.")
endif ()


MESSAGE ( STATUS "CXX STANDARD ${CMAKE_CXX_STANDARD}" )
MESSAGE ( STATUS "C COMPILER ${CMAKE_C_COMPILER}" )
MESSAGE ( STATUS "CXX COMPILER ${CMAKE_CXX_COMPILER}" )
MESSAGE ( STATUS "CMAKE BUILD ${CMAKE_BUILD_TYPE}" )

add_subdirectory(thirdparty/catch)

MESSAGE ( STATUS "INC DIR : ${CATCH_LIBRARIES} " )

include_directories( ${CATCH_INCLUDE_DIR} )

add_executable(main_program main.cpp common.cpp common.hpp)
add_executable(main_test test.cpp common.cpp common.hpp)

target_link_libraries(main_test ${CATCH_LIBRARIES})

enable_testing(true)
add_test(NAME main_test 
    COMMAND main_test)

