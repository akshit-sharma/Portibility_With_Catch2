#/bin/bash
# build the project

ORIG=$PWD
TEST=main_test


DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd $DIR/BUILD/
./${TEST}

if [ $? -ne 0 ]; then
    echo "testing error in $C_COMPILER $CXX_COMPILER $BUILD_CONFIG"
    exit 4
fi
echo "====================================================================================="


cd $ORIG
