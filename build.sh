#/bin/bash
# build the project

ORIG=$PWD

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

mkdir -p $DIR/BUILD/
cd $DIR/BUILD/
cmake -DCMAKE_C_COMPILER=$C_COMPILER -DCMAKE_CXX_COMPILER=$CXX_COMPILER\
     -DCMAKE_BUILD_TYPE=$BUILD_CONFIG $DIR
make -j5
if [ $? -ne 0 ]; then
    echo "Building error in $C_COMPILER $CXX_COMPILER $BUILD_CONFIG"
    exit 4
fi
echo "====================================================================================="


cd $ORIG
