cmake_minimum_required(VERSION 3.9)
project(catch_builder CXX)
include(ExternalProject)
find_package(Git REQUIRED)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CATCH_ROOT ${CMAKE_BINARY_DIR}/thirdparty/catch)

ExternalProject_Add(Catch-External
   PREFIX ${CATCH_ROOT}
   GIT_REPOSITORY "https://github.com/catchorg/Catch2.git"
   TIMEOUT 10
   UPDATE_COMMAND ""
   CONFIGURE_COMMAND ""
   BUILD_COMMAND ""
   INSTALL_COMMAND ""
   LOG_DOWNLOAD ON
   )

ExternalProject_Get_Property(Catch-External source_dir)
set(CATCH_INCLUDE_DIR ${source_dir}/single_include CACHE INTERNAL "Path to include folder for Catch" )

add_library(Catch INTERFACE)
add_dependencies(Catch Catch-External)
target_include_directories(Catch INTERFACE ${CATCH_INCLUDE_DIR})
set(CATCH_LIBRARIES Catch CACHE INTERNAL "INTERFACE With hpp files")

