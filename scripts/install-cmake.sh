#/bin/bash
# script for installing cmake-3.10

WEB_SOURCE=https://cmake.org/files/v3.10/cmake-3.10.2.tar.gz
FILE=cmake-3.10.2
MAJ_VER=3
MIN_VER=10

MAJOR=`cmake --version  | grep version | cut -d" " -f 3 | \
      sed -re "s/^([0-9]+)\.([0-9]+).*$/\1/g"`

MINOR=`cmake --version  | grep version | cut -d" " -f 3 | \
      sed -re "s/^([0-9]+)\.([0-9]+).*$/\2/g"`

#echo "Checking $MAJ_VER < $MAJOR"
if [ $MAJ_VER -lt $MAJOR ]; then
  echo "CMake $MAJOR.$MINOR installed"
  exit 0
fi
if [ $MAJ_VER -eq $MAJOR ]; then
#  echo "Checking $MIN_VER < $MINOR"
  if [ $MIN_VER -le $MINOR ]; then
    echo "CMake $MAJOR.$MINOR installed"
    exit 0
  fi  
fi

mkdir -p SCRIPTS
cd SCRIPTS
wget ${WEB_SOURCE} -q -O ${FILE}.tar.gz > cmake_download_log

mkdir ${FILE}
tar zxf ${FILE}.tar.gz 
cd ${FILE}
./bootstrap
make -j 4
make install

cd .. # ${FILE}
cd .. # SCRIPTS

