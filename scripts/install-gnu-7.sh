#/bin/bash
# script for installing cmake-3.10

WEB_SOURCE=http://mirrors-usa.go-parts.com/gcc/releases/gcc-7.2.0/gcc-7.2.0.tar.gz
FILE=gcc-7.2.0
MAJ_VER=7
MIN_VER=2

MAJOR=`cmake --version  | head -n1 | cut -d" " -f 3 | \
      sed -re "s/^([0-9]+)\.([0-9]+).*$/\1/g"`

MINOR=`cmake --version  | head -n1 | cut -d" " -f 3 | \
      sed -re "s/^([0-9]+)\.([0-9]+).*$/\2/g"`

#echo "Checking $MAJ_VER < $MAJOR"
if [ $MAJ_VER -lt $MAJOR ]; then
  echo "CMake $MAJOR.$MINOR installed"
  exit 0
fi
if [ $MAJ_VER -eq $MAJOR ]; then
#  echo "Checking $MIN_VER < $MINOR"
  if [ $MIN_VER -le $MINOR ]; then
    echo "CMake $MAJOR.$MINOR installed"
    exit 0
  fi  
fi

mkdir -p SCRIPTS
cd SCRIPTS
wget ${WEB_SOURCE} -q -O ${FILE}.tar.gz > cmake_download_log

mkdir ${FILE}
tar zxf ${FILE}.tar.gz 
cd ${FILE}
./contrib/download_prerequisites
mkdir objdir
cd objdir
$PWD/../${FILE}/configure --prefix=$HOME/${FILE} --enable-languages=c,c++
make -j 4
make install

cd .. # objdir
cd .. # ${FILE}
cd .. # SCRIPTS

