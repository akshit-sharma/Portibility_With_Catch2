#/bin/bash
# build the project

ORIG=$PWD
EXE=main_program


DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd $DIR/BUILD/
./${EXE}

if [ $? -ne 0 ]; then
    echo "testing error in $C_COMPILER $CXX_COMPILER $BUILD_CONFIG"
    exit 4
fi
echo "====================================================================================="


cd $ORIG
